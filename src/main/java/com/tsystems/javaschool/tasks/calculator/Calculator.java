package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (isNotValid(statement)) {
            return null;
        }

        ArrayDeque<Float> operands = new ArrayDeque<>();
        ArrayDeque<Character> operators = new ArrayDeque<>();

        try {
            for (int i = 0; i < statement.length(); i++) {

                char c = statement.charAt(i);
                if (c == '(') {
                    operators.add(c);

                } else if (c == ')') {

                    while (operators.getLast() != '(') {
                        process(operands, operators.removeLast());
                    }

                    operators.removeLast();

                } else if (c == '+' || c == '-' || c == '*' || c == '/') {

                    if (!operators.isEmpty() && getPriority(operators.getLast()) >= getPriority(c)) {
                        process(operands, operators.removeLast());
                    }

                    operators.add(c);

                } else {
                    StringBuilder s = new StringBuilder();
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                        s.append(statement.charAt(i++));
                    }
                    --i;
                    operands.add(Float.parseFloat(s.toString()));
                }
            }
            while (!operators.isEmpty()) {
                process(operands, operators.removeLast());
            }
        } catch (NumberFormatException | ArithmeticException | UnsupportedOperationException e) {
            return null;
        }

        float result = operands.getLast();

        if (result % 1 == 0) {
            return format(result);
        } else {
            return format(round(result));
        }
    }

    private int getPriority(char character) {
        if (character == '*' || character == '/') {
            return 1;
        } else if (character == '+' || character == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    private boolean isNotValid(String expression) {
        if (expression == null || expression.isEmpty() || expression.contains("--")
                || expression.contains("++") || expression.contains("//")
                || expression.contains("**") || expression.contains("..")) {

            return true;
        }

        int count = 0;
        for (char c : expression.toCharArray()) {
            if (c == '(') {
                count++;
            } else if (c == ')') {
                count--;
                if (count < 0)
                    return true;
            }
        }

        return count != 0;

    }


    private void process(ArrayDeque<Float> operands, char operator) {
        float oper1 = operands.removeLast();
        float oper2 = operands.removeLast();
        switch (operator) {
            case '+':
                operands.add(oper1 + oper2);
                break;
            case '-':
                operands.add(oper2 - oper1);
                break;
            case '*':
                operands.add(oper1 * oper2);
                break;
            case '/':
                if (oper1 == 0)
                    throw new ArithmeticException();
                else
                    operands.add(oper2 / oper1);
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    private String format(float d) {
        if (d == (long) d) {
            return String.format(Locale.US, "%d", (long) d);
        } else {
            return String.format(Locale.US, "%s", d);
        }
    }

    private float round(float value) {
        value *= 10000;
        long tmp = Math.round(value);
        return (float) tmp / 10000;
    }

}












