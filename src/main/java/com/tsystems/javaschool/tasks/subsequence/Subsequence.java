package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        boolean checked;
        for (Object objx : x) {

            checked = false;
            while (!y.isEmpty()) {
                if (objx == y.get(0)) {
                    checked = true;
                }

                y.remove(0);

                if (checked) {
                    break;
                }
            }
            if (checked) {
                continue;
            }

            return false;
        }
        return true;
    }
}
