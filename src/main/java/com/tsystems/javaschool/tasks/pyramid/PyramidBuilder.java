package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();
        int height = 0;
        while (size > 0) {
            height++;
            size = size - height;
        }
        if (size != 0) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int[][] result = new int[height][height * 2 - 1];
        int position;
        int count = 0;
        for (int i = 0; i < height; i++) {
            position = height - i;
            for (int j = 0; j < i + 1; j++) {
                result[i][position - 1] = inputNumbers.get(count);
                count++;
                position += 2;
            }
        }

        return result;
    }

}
